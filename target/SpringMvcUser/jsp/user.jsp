<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Registration</title>
</head>
<body>
<div>
   <center><h3> Patient Information </h3></center> 
   <center>
    <table>
    	<tr>
    		<td>
    			<form action="registerProcess" modelAttribute="user" method="post" enctype="multipart/form-data">
     			Patient Name: <input type = "text" id="name" name="name" />
     		</td>
     	</tr>
   		<tr>
   			<td> 
   				Gender:<select id="gender" name="gender">
   				<option value=""></option>
				<option value="Male">Male</option>
				<option value="Female">Female</option>
				</select>
			</td>
			<td>
				Age:<select name="age" id="age" >
				<option value=""></option>
				<option value="age1">Age 18</option>
				<option value="age2">Between 18-25</option>
				<option value="age3">Between 25-30</option>
				<option value="age4">Between 30-35</option>
				<option value="age5">Between 35-40</option>
				<option value="age6">More Then 40</option>
			</td>
		</tr>
			<tr>
				<td>
					<h2>Current health</h2>
				</td>
			</tr>
			<tr>
				<td>
					Hypertension:<select name="hypertension"  id="hypertension" >
					<option value=""></option>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
					</select>
				</td>
				<option value=""></option>
				<td>
					BloodPressure:<select name="bloodPressure" id="bloodPressure"  >
					<option value=""></option>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					BloodSugar:<select name="bloodSugar" id="bloodSugar" >
					<option value=""></option>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
					</select>
				</td>
				<td>
					Overweight:<select name="overweight" id="overweight" >
					<option value=""></option>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td><h2>Habbits</h2></td>
			</tr>
			<tr>
				<td>
					Smoking:<select name="smoking" id="smoking" >
					<option value=""></option>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
					</select>
				</td>
				<td>
					Alcohal:<select name="alcohol" id="alcohol" >
					<option value=""></option>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					Daily Exercise:<select name="dailyExercise" id="dailyExercise" >
					<option value=""></option>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
					</select>
				</td>
				<td>
					Drugs:<select name="drugs" id="drugs"  >
					<option value=""></option>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
					</select>
				</td>
			</tr>
			
		
			
					<center><input type="submit" value=calculate /></center>
			
    	</table>
    	</center>
    </form>
</div>
</body>
</html>