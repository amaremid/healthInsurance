package jbr.springmvc.dao;
import jbr.springmvc.model.User;

public class UserDaoImpl implements UserDao {
	
	public double calculateHI(User user) {
		double base=5000;
		double hi=5000;
		
		if((user.getAge()>=18 && user.getAge()<25)|| user.getAge()>25 ) {
			
			hi=base+(base*10)/100;
			if((user.getAge()>=25 && user.getAge()<30) || user.getAge()>30) {
				 hi=hi+(hi*10)/100;
				 
				 if((user.getAge()>=30 && user.getAge()<35) || user.getAge()>35) {
					 hi=hi+(hi*10)/100;
					 
					 if((user.getAge()>=35 && user.getAge()<40) || user.getAge()>40) {
						 hi=hi+(hi*10)/100;
						 
						 if(user.getAge()>=40) {
							 hi=hi+(hi*20)/100;
							 if(user.getAge()>45) {
								 int temp=user.getAge()-45;
								 int res=temp/5;
								 int rem=temp%5;
								 if(res>0) {
									 for(int i=res;i>0;i--) {
										 hi=hi+(hi*20)/100;
									 }
									 if(rem>0) {
										 
										 hi=hi+(hi*20)/100;
										 
									 }
									 
								 }
								 
							 }
							 
						 }
						 
					 }
				 }
			}
		}
			
		
		if(user.getGender()=="male")
		{
			hi=hi+(hi*2)/100;
		}
		
		if(user.getHypertension()=="yes") {
			hi=hi+(hi*1)/100;
		}
		if(user.getBloodPressure()=="yes") {
			hi=hi+(hi*1)/100;
		}
		if(user.getBloodSugar()=="yes") {
			hi=hi+(hi*1)/100;
		}
		if(user.getOverweight()=="yes") {
			hi=hi+(hi*1)/100;
		}
		
		if(user.getDailyExercise()=="yes") {
			hi=hi-(hi*3)/100;
		}
		if(user.getSmoking()=="yes") {
			hi=hi+(hi*3)/100;
		}
		if(user.getAlcohol()=="yes") {
			hi=hi+(hi*3)/100;
		}
		if(user.getDrugs()=="yes") {
			hi=hi+(hi*3)/100;
		}
		return hi;
	}

}
