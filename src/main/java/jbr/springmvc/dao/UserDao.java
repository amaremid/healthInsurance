package jbr.springmvc.dao;

import jbr.springmvc.model.User;

public interface UserDao {
	public double calculateHI(User user);
}
