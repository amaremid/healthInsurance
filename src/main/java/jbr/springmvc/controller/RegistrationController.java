package jbr.springmvc.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import jbr.springmvc.dao.UserDao;
import jbr.springmvc.model.User;

@Controller
public class RegistrationController {
	@Autowired
	public UserDao userDao;
	
	@RequestMapping(value="/register")
	public ModelAndView showRegisterForm(HttpServletRequest request,HttpServletResponse response) {
		ModelAndView mav=new ModelAndView("user");
		mav.addObject("user",new User());
		return mav;
	}
	@RequestMapping(value="/registerProcess")
	public ModelAndView calculateI(HttpServletRequest request,HttpServletResponse response,@ModelAttribute User user,Model model) {
		double healthInsurance=userDao.calculateHI(user);
		model.addAttribute(user);
		return new ModelAndView("welcome","healthInsurance",healthInsurance);
		
	}

}

